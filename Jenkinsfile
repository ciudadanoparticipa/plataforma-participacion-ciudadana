#!groovy

node('linux-docker') {

    def REGISTRY_IMAGE_URL="registry.gitlab.com/ciudadanoparticipa/plataforma-participacion-ciudadana";
    def WEBHOOKS_ID="plataforma-participacion-ciudadana";
    def WEBHOOKS_URL="http://192.168.129.225:9999";

    stage('Preparation') {
        checkout scm;
        env.NODEJS_HOME = "${tool 'node-v8.9.4'}";
        env.PATH="${env.NODEJS_HOME}/bin:${env.PATH}";
    }

    stage('Install packages') {
        sh "npm i";
    }

    stage('Build') {
        if (env.BRANCH_NAME == 'test') {
            sh "npm run build -- --mode test";
        } else {
            sh "npm run build";
        }
    }

    stage('Docker build') {
        def imageName = "${REGISTRY_IMAGE_URL}:${env.BRANCH_NAME}";
        if (env.BRANCH_NAME == 'master') {
            imageName = "${REGISTRY_IMAGE_URL}:latest";
        }

        docker.withRegistry('https://registry.gitlab.com', 'registry.gitlab.com-pass') {
            def customImage = docker.build(imageName);
            customImage.push();
        }
    }

    stage('Api Docker build') {
        dir ('./api/') {
            def imageName = "${REGISTRY_IMAGE_URL}/api:${env.BRANCH_NAME}";
            if (env.BRANCH_NAME == 'master') {
                imageName = "${REGISTRY_IMAGE_URL}/api:latest";
            }

            docker.withRegistry('https://registry.gitlab.com', 'registry.gitlab.com-pass') {
                def customImage = docker.build(imageName);
                customImage.push();
            }
        }
    }

    stage('Backups Server Docker build') {
        dir ('./backups_server/') {
            def imageName = "${REGISTRY_IMAGE_URL}/backups_server:${env.BRANCH_NAME}";
            if (env.BRANCH_NAME == 'master') {
                imageName = "${REGISTRY_IMAGE_URL}/backups_server:latest";
            }

            docker.withRegistry('https://registry.gitlab.com', 'registry.gitlab.com-pass') {
                def customImage = docker.build(imageName);
                customImage.push();
            }
        }
    }

    stage('Aprovisioning') {
        if (env.BRANCH_NAME == 'devel') {
            sh "(cd /opt/containers/ciudadano-participa/ && docker-compose stop && docker-compose up -d)"
        }
        if (env.BRANCH_NAME == 'master') {
            sh "curl ${WEBHOOKS_URL}/hooks/${WEBHOOKS_ID}"
        }
        if (env.BRANCH_NAME == 'test') {
            sh "curl ${WEBHOOKS_URL}/hooks/${WEBHOOKS_ID}-test"
        }
    }

}
