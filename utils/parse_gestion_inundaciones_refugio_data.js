#!/bin/node

const path = require('path')
const chalk = require('chalk')
const CsvReadableStream = require('csv-reader')
const { getCSVFile, getRefugios2017Title, saveRefugios2017File, getGeoreferencia, getCentroMunicipalData, sumValueToLatLon } = require('./_common')

const srcPath = path.join(__dirname, '..', 'src')
const inputStreamPath = process.argv[2]
const refugioFilePath = process.argv[3] || path.join(srcPath, 'assets', 'json', 'refugios.json')

if (!inputStreamPath || !refugioFilePath) {
  console.error(chalk.red('Se necesita 2 argumentos, el path del csv con los datos estadísticos y el path del archivo json donde serán cargados estos datos'))
  process.exit(1)
}

const inputStream = getCSVFile(inputStreamPath)

const refugios = {}
let currentGroup = ''

const rowLimit = 116
const rowTitlesPosition = 4
const titles = []

let rowIdx = 0
let colCentroMunicipalPosition = 2
let colRefugioNamePosition = 4
let colGeoreferenciaPosition = 6
let elements
let lastRefugio = {}

inputStream
  .pipe(CsvReadableStream({ parseNumbers: true, parseBooleans: true, trim: true }))
  .on('data', row => {
    rowIdx++
    elements = row.filter(element => element).length
    if (!elements || rowIdx >= rowLimit) return
    if (rowIdx === rowTitlesPosition) {
      row.forEach((column, idx) => {
        currentGroup = column.trim()
        if (currentGroup) {
          titles[idx] = getRefugios2017Title(currentGroup)
        }
      })
    } else if (elements > 1 && row[colRefugioNamePosition]) {
      refugios[row[colRefugioNamePosition]] = {}
      row.forEach((column, idx) => {
        if (column) {
          refugios[row[colRefugioNamePosition]][titles[idx]] = idx === colCentroMunicipalPosition ? getCentroMunicipalData(column) : idx === colGeoreferenciaPosition ? getGeoreferencia(column) : column
        } else {
          if (lastRefugio[titles[idx]]) {
            if (idx === colGeoreferenciaPosition) {
              refugios[row[colRefugioNamePosition]][titles[idx]] = sumValueToLatLon(lastRefugio[titles[idx]])
            } else {
              refugios[row[colRefugioNamePosition]][titles[idx]] = lastRefugio[titles[idx]]
            }
          } else {
            refugios[row[colRefugioNamePosition]][titles[idx]] = column
          }
        }
      })
      lastRefugio = refugios[row[colRefugioNamePosition]]
    }
  })
  .on('end', () => {
    saveRefugios2017File(refugios, refugioFilePath)
  })
