#!/bin/node

const path = require('path')
const chalk = require('chalk')
const CsvReadableStream = require('csv-reader')
const {
  clone,
  getFileToJSON,
  printRefugioNameCoincidenceNumberObj,
  getCSVFile,
  getRefugiosStatisticTitle,
  getRefugioNameInObjectOrParams,
  saveStatisticRefugiosFile
} = require('./_common')

const srcPath = path.join(__dirname, '..', 'src')
const inputStreamPath = process.argv[2]
const refugioFilePath = process.argv[3] || path.join(srcPath, 'assets', 'json', 'refugios.json')

if (!inputStreamPath || !refugioFilePath) {
  console.error(chalk.red('Se necesita 2 argumentos, el path del csv con los datos estadísticos y el path del archivo json donde serán cargados estos datos'))
  process.exit(1)
}

const refugios = getFileToJSON(refugioFilePath)
const inputStream = getCSVFile(inputStreamPath)

const rowTitlesPosition = 1
const columnLimit = 19
const listaDeRefugiosSinDatosGeograficos = []

let titles = []
let currentGroup = ''
let refugiosToSave = clone(refugios)
let statisticData = {}

let rowIdx = 0
const colRefugioNamePosition = 1
const colComposicionFamiliarPositionStart = 7
const colComposicionFamiliarPositionEnd = 12
const colMaterialesEntregadosPositionStart = 13
const colMaterialesEntregadosPositionEnd = 17

let elements

inputStream
  .pipe(CsvReadableStream({ parseNumbers: true, parseBooleans: true, trim: true }))
  .on('data', row => {
    rowIdx++
    elements = row.length
    if (!elements || elements < columnLimit) return
    if (rowIdx === rowTitlesPosition) {
      row.forEach((column, idx) => {
        currentGroup = column.trim()
        if (currentGroup) {
          titles[idx] = getRefugiosStatisticTitle(currentGroup)
        }
      })
    } else if (row[colRefugioNamePosition]) {
      const nombreRefugio = getRefugioNameInObjectOrParams(refugiosToSave, row[colRefugioNamePosition])
      if (refugiosToSave[nombreRefugio]) {
        if (!statisticData[nombreRefugio]) statisticData[nombreRefugio] = {}
        if (!statisticData[nombreRefugio]['cantidadDeFamilias']) statisticData[nombreRefugio]['cantidadDeFamilias'] = 0
        statisticData[nombreRefugio]['cantidadDeFamilias']++
        row.forEach((column, idx) => {
          if (!column || !titles[idx]) return
          if (!statisticData[nombreRefugio]['composicionFamiliar']) statisticData[nombreRefugio]['composicionFamiliar'] = {}
          if (!statisticData[nombreRefugio]['materialesEntregados']) statisticData[nombreRefugio]['materialesEntregados'] = {}
          if (idx >= colComposicionFamiliarPositionStart && idx < colComposicionFamiliarPositionEnd) {
            if (!statisticData[nombreRefugio]['composicionFamiliar'][titles[idx]]) statisticData[nombreRefugio]['composicionFamiliar'][titles[idx]] = 0
            statisticData[nombreRefugio]['composicionFamiliar'][titles[idx]] += isNaN(column) ? 0 : parseInt(column)
          } else if (idx >= colMaterialesEntregadosPositionStart && idx < colMaterialesEntregadosPositionEnd) {
            if (!statisticData[nombreRefugio]['materialesEntregados'][titles[idx]]) statisticData[nombreRefugio]['materialesEntregados'][titles[idx]] = 0
            statisticData[nombreRefugio]['materialesEntregados'][titles[idx]] += isNaN(column) ? 0 : parseInt(column)
          }
        })
      } else if (nombreRefugio && !listaDeRefugiosSinDatosGeograficos.find(e => e === nombreRefugio)) {
        listaDeRefugiosSinDatosGeograficos.push(nombreRefugio)
      }
    }
  })
  .on('end', () => {
    printRefugioNameCoincidenceNumberObj()
    Object.keys(statisticData).forEach(key => {
      refugiosToSave[key]['estadisticas'] = clone(statisticData[key])
    })
    console.log('\nLista de refugios que no poseen información de geolocalización: \n', listaDeRefugiosSinDatosGeograficos)
    console.log('\n\nLista de refugios sin estadísticas: \n', Object.values(refugiosToSave).filter(r => !r['estadisticas']).map(r => r.nombre))
    saveStatisticRefugiosFile(refugiosToSave, refugioFilePath)
  })
