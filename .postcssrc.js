module.exports = {
  plugins: {
    'postcss-cssnext': {
      'features': {
        'autoprefixer': { 'grid': true }
      }
    },
    'postcss-vmax': {}
  }
}
