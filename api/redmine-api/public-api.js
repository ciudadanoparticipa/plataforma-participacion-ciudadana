const fs = require('fs')
const chalk = require('chalk')
const HttpStatus = require('http-status-codes')
const polygonCenter = require('geojson-polygon-center')
const multer = require('multer')
const { DOMParser } = require('xmldom')
const togeojson = require('@mapbox/togeojson')
const { diff } = require('deep-diff')
const RedmineApi = require('./redmine-api')
const { stringify, clone, JSONRubyToJS, findByName, findIdByName, formatDate, HttpError, __PATH_BASE } = require('../utils')

const ACTIVE_PROJECT_STATUS = 1

const CLOSE_PROJECT_STATUS = 5

const FILES_PATH = `${__PATH_BASE}public/projects`

const getActiveProjects = projects => projects.filter(project => project.status === ACTIVE_PROJECT_STATUS)

const getRemoveUnusedFiles = (auditoriums, projectCode, fieldname) => {
  let filePathToDeleteList = []
  if (auditoriums && auditoriums.datos) {
    const fileToAddOrDeleteAudit = auditoriums.datos
      .filter(a => (a.path.includes(fieldname) && a.kind === 'A'))
      .map(a => a.item)
    const fileToAddAudit = fileToAddOrDeleteAudit
      .filter(a => a.kind !== 'D')
    const fileToEditAudit = auditoriums.datos
      .filter(e => (e.path.includes(fieldname) && e.kind === 'E'))
      .filter(e => fileToAddAudit.length ? fileToAddAudit.find(a => a.rhs !== e.lhs) : true)
    const fileToDeleteAudit = fileToAddOrDeleteAudit
      .filter(d => d.kind === 'D')
      .filter(d => fileToEditAudit.length ? fileToEditAudit.find(e => e.rhs !== d.lhs) : true)
    const fileToEditOrDeleteAudit = []
    fileToEditOrDeleteAudit.push(...fileToEditAudit)
    fileToEditOrDeleteAudit.push(...fileToDeleteAudit)
    fileToEditOrDeleteAudit.forEach(audit => {
      const isAddInOtherPosition = fileToEditOrDeleteAudit.find(a => a.rhs === audit.lhs)
      if (!isAddInOtherPosition) {
        const filePathToDelete = `${FILES_PATH}/${projectCode}/${fieldname}/${audit.lhs}`
        if (!filePathToDeleteList.includes(filePathToDelete)) filePathToDeleteList.push(filePathToDelete)
      }
    })
  }
  return filePathToDeleteList
}

const removeUnusedFiles = unusedFiles => {
  unusedFiles.forEach(filePathToDelete => {
    fs.unlink(filePathToDelete, (err) => {
      if (err) return console.warn(chalk.yellow('El archivo no existe o ya fue eliminado'), chalk.red(err))
      console.log(chalk.yellow(`${filePathToDelete} fue eliminado`))
    })
  })
}

const createUnexistFolders = code => {
  if (code) {
    if (!fs.existsSync(FILES_PATH)) fs.mkdirSync(FILES_PATH);
    const folderPath = `${FILES_PATH}/${code}/`
    if (!fs.existsSync(folderPath)) fs.mkdirSync(folderPath);
    (['geojson', 'images', 'kml', 'shapes', 'pin', 'cierre', 'planificacion', 'ejecucion', 'tooltip']).forEach(folderName => {
      const folderPath = `${FILES_PATH}/${code}/${folderName}/`
      if (!fs.existsSync(folderPath)) fs.mkdirSync(folderPath)
    })
  }
}

const storage = multer.diskStorage({
  destination (req, file, cb) {
    if (req.body.data) {
      const { code } = JSON.parse(req.body.data)
      createUnexistFolders(code)
      if (!code) return cb(new HttpError('El código del proyecto no fue enviado desde el formulario', HttpStatus.BAD_REQUEST))
      return cb(null, `${FILES_PATH}/${code}/${file.fieldname}/`)
    }
    cb(new HttpError('Datos de proyectos no enviados desde el formulario', HttpStatus.BAD_REQUEST))
  },
  filename (req, file, cb) {
    const extention = file.originalname.split('.')[1]
    if (extention) {
      switch (file.fieldname) {
        case 'kml':
          cb(null, `kml.${extention}`)
          break
        case 'shapes':
          cb(null, `01.${extention}`)
          break
        case 'pin':
          cb(null, `${file.fieldname}.${extention}`)
          break
        case 'cierre':
          cb(null, `${file.fieldname}.${extention}`)
          break
        case 'planificacion':
          cb(null, `${file.fieldname}.${extention}`)
          break
        case 'ejecucion':
          cb(null, `${file.fieldname}.${extention}`)
          break
        case 'tooltip':
          cb(null, `${file.fieldname}.${extention}`)
          break
        default:
          cb(null, file.originalname)
          break
      }
    } else cb(new HttpError(`El archivo ${file.originalname} no tiene extención o tipo`, HttpStatus.BAD_REQUEST))
  }
})

const saveGeoJsonAndGetCenterCoords = ({ code, color, descripcion, name, number }, kml) => {
  if (kml) {
    const kmlObj = new DOMParser().parseFromString(fs.readFileSync(kml.path, 'utf8'))
    const geojson = togeojson.kml(kmlObj)
    if (geojson && geojson.features) {
      geojson.features = geojson.features.map((f, i) => {
        f.properties = {
          ...f.properties,
          code,
          color,
          fill: color,
          name,
          number,
          description: descripcion,
          parent: i === 0,
          stroke: color
        }
        return f
      })
      const polygon = geojson.features[0]
      const geojsonFilePath = `${FILES_PATH}/${code}/geojson/geojson.geojson`
      const geojsonFile = stringify(geojson, 0)
      fs.writeFile(geojsonFilePath, geojsonFile, 'utf8', err => {
        if (err) throw new HttpError('El geojson no pudo ser guardado', HttpStatus.INTERNAL_SERVER_ERROR)
        console.log(chalk.yellow(`${geojsonFilePath} fue actualizado`))
      })
      return polygon ? polygonCenter(polygon.geometry) : null
    }
  }
  return null
}

module.exports = class Redmine extends RedmineApi {
  constructor (config, request) {
    super(config, request)
    const { statusId } = config
    this.statusId = statusId
  }

  setInitialValues () {
    super.setInitialValues()
    this.getTrackers().then(data => { console.log(chalk.yellow('Cargado desde Redmine: Trackers')) })
    this.getCustomFields().then(data => { console.log(chalk.yellow('Cargado desde Redmine: Custom Fields')) })
    // print environment info
    console.log(chalk.yellow(`\nConnecting to PublicRedmineApi at ${this.config.url}`))
    console.log(chalk.yellow(`Using PublicRedmineApi API Key: ${this.options.apiKey}\n`))
  }

  getTrackers () {
    if (this._cache.trackers) return Promise.resolve(this._cache.trackers)
    return this._getTrackers().then(data => { this._cache.trackers = data; return data }, this._onError)
  }

  getCustomFields () {
    if (this._cache.customFields) return Promise.resolve(this._cache.customFields)
    return this._getCustomFields().then(data => { this._cache.customFields = data; return data }, this._onError)
  }

  getProjects (token, filterClosedProjects = true) {
    const opts = token ? { token } : {}
    return this._api(this._getUrl('projects', opts))
      .then(data => data.projects)
      .then(projects => filterClosedProjects ? getActiveProjects(projects) : projects)
      .then(projects => projects.map(project => this._loadProjectData(project)))
  }

  getProject (token, code) {
    return this._api(this._getUrl(`projects/${code}`, { token }))
      .then(data => this._loadProjectData(data.project))
  }

  getProjectAuditorias (token, code) {
    return this._api(this._getUrl(`projects/${code}`, { token }))
      .then(data => this._getProjectAuditorias(data.project))
  }

  getNewNumber () {
    return this.getProjects(null, false).then(projects => projects.length + 1)
  }

  updateProjects (projectList) {
    return Promise.all(projectList.map(project => this.updateProject(this.config.apiKey, project)))
  }

  updateProject (token, project, files) {
    const centerCoords = saveGeoJsonAndGetCenterCoords(project, files && files.kml && files.kml[0])
    return Promise.all([this.getProjectAuditorias(token, project.code), this.getCustomFields()])
      .then(([auditorias, customFields]) => {
        let auditoriums = auditorias || []
        if (centerCoords) {
          project.mapProperties = {
            center: centerCoords.coordinates,
            mapIconPosition: centerCoords.coordinates,
            parent: true,
            zoom: 13
          }
        }
        if (project.auditoria) {
          const auditoria = clone(project.auditoria)
          if (auditoria) {
            auditoriums.push(auditoria)
            removeUnusedFiles(getRemoveUnusedFiles(auditoria, project.code, 'images'))
            delete project.auditoria
          }
          return this._saveProject(project, token, auditoriums, customFields)
        } else {
          return Promise.all([this.getCurrentUser(), this.getProject(token, project.code)])
            .then(([dataUser, oldDataProject]) => {
              if (dataUser.user) {
                if (oldDataProject && oldDataProject.projectInfo) {
                  const oldProject = clone(oldDataProject.projectInfo)
                  delete oldProject.identifier
                  delete oldProject.assigned_to_id
                  auditoriums.push({
                    usuario: dataUser.user,
                    fecha: formatDate(),
                    data: diff(oldProject, project)
                  })
                  return this._saveProject(project, token, auditoriums, customFields)
                }
              }
              throw new HttpError('El usuario para guardar la auditoria no existe', HttpStatus.NOT_FOUND)
            })
        }
      })
  }

  createProject (token, project, files) {
    const centerCoords = saveGeoJsonAndGetCenterCoords(project, files && files.kml && files.kml[0])
    return Promise.all([this.getCustomFields(), this.getNewNumber()])
      .then(([customFields, newNumber]) => {
        if (newNumber) project.number = newNumber
        if (centerCoords) {
          project.mapProperties = {
            center: centerCoords.coordinates,
            mapIconPosition: centerCoords.coordinates,
            parent: true
          }
        }
        let auditoriums = [clone(project.auditoria)]
        delete project.auditoria
        const data = {
          token,
          body: {
            project: {
              name: project.name,
              identifier: project.code,
              description: project.descripcion,
              homepage: `http://asuparticipa.asuncion.gov.py/map?code=${project.code}`,
              is_public: false,
              parent_id: null,
              inherit_members: true,
              custom_fields: this._getProjectCustomFields(project, auditoriums, customFields)
            }
          },
          method: 'POST'
        }
        return this._api(this._getUrl(`projects`, data)).then(response => {
          response.project.number = project.number
          return response
        })
      })
  }

  deleteProject (token, code) {
    return this._api(this._getUrl(`projects/${code}/close`, {
      token,
      body: {
        project: {
          status: CLOSE_PROJECT_STATUS
        }
      },
      method: 'POST'
    }))
  }

  saveAllFiles () {
    return multer({ storage })
      .fields([
        { name: 'images' },
        { name: 'shapes' },
        { name: 'pin' },
        { name: 'cierre' },
        { name: 'planificacion' },
        { name: 'ejecucion' },
        { name: 'tooltip' },
        { name: 'kml' }
      ])
  }

  sendFile ({ path }) {
    if (!path) return null
    const file = fs.readFileSync(path)
    return this._api(this._getUrl('uploads', { body: file, method: 'POST', json: false, headers: { 'Content-Type': 'application/octet-stream' } }))
  }

  createIssue (body, uploads) {
    const data = {
      subject: body.subject,
      description: body.message,
      project_id: body.project_id,
      status_id: this.statusId,
      is_private: true,
      assigned_to_id: body.assigned_to_id,
      custom_fields: [],
      uploads
    }
    return Promise.all([this.getTrackers(), this.getCustomFields()])
      .then(([trackers, customFields]) => {
        data.tracker_id = findIdByName(trackers, body.tipoParticipacion, 'Tracker')
        data.custom_fields.push({
          id: findIdByName(customFields, this.config.customFields.NOMBRE, 'CustomField'),
          value: body.name
        })
        data.custom_fields.push({
          id: findIdByName(customFields, this.config.customFields.EMAIL, 'CustomField'),
          value: body.email
        })
        data.custom_fields.push({
          id: findIdByName(customFields, this.config.customFields.LATITUD, 'CustomField'),
          value: body.latitude
        })
        data.custom_fields.push({
          id: findIdByName(customFields, this.config.customFields.LONGITUD, 'CustomField'),
          value: body.longitude
        })
        if (body.tipoReclamo) {
          data.custom_fields.push({
            id: findIdByName(customFields, this.config.customFields.TIPO_RECLAMO, 'CustomField'),
            value: body.tipoReclamo
          })
        }
        const options = this._getUrl('issues', { body: { issue: data }, method: 'POST' })
        return this._api(options)
      })
  }

  getIssueByUUID (projectId, uuid) {
    const self = this
    const includes = 'include=journals,attachments'
    return this.getCustomFields()
      .then(customFields => {
        const identifierId = findIdByName(customFields, this.config.customFields.ISSUE_IDENTIFIER, 'CustomField')
        return this._getUrl('issues', { query: `project_id=${projectId}&status_id=*&cf_${identifierId}=${uuid}&limit=1000&${includes}` })
      })
      .then(options => this._api(options))
      .then(data => data.issues)
      .then(issues => issues && issues.length ? issues : [])
      .then(issues => issues.find(issue => self._findIssueByUUID(issue, uuid)))
      .then(issue => {
        if (issue && issue.id) return issue.id
        else throw new HttpError('La petición que busca no existe o fue borrada', HttpStatus.NOT_FOUND)
      })
      .then(issueId => this._api(this._getUrl(`issues/${issueId}`, { query: includes })))
      .then(data => {
        data.issue.latitude = data.issue.custom_fields.find(c => c.name === this.config.customFields.LATITUD).value
        data.issue.longitude = data.issue.custom_fields.find(c => c.name === this.config.customFields.LONGITUD).value
        return data.issue
      })
  }

  getCurrentUser () {
    if (this._cache.currentUser) return Promise.resolve(this._cache.currentUser)
    return this._getCurrentUser().then(data => { this._cache.currentUser = data; return data }, this._onError)
  }

  _getCurrentUser () {
    return this._api(this._getUrl('users/current', {}, { addToken: false }))
  }

  _saveProject (project, token, auditoriums, customFields) {
    const data = {
      token,
      body: {
        project: {
          custom_fields: this._getProjectCustomFields(project, auditoriums, customFields)
        }
      },
      method: 'PUT'
    }
    return this._api(this._getUrl(`projects/${project.id}`, data))
  }

  _getCustomFields () { return this._api(this._getUrl('custom_fields')).then(data => data.custom_fields) }

  _getTrackers () { return this._api(this._getUrl('trackers')).then(data => data.trackers) }

  _findIssueByUUID (issue, uuid) { return issue.custom_fields.find(cf => cf.value === uuid) }

  _JSONRubyToJSByCustomFieldsName (data, customFieldsName) {
    const json = findByName(data.custom_fields, this.config.customFields[customFieldsName], 'CustomField')
    if (json && json.value) return JSONRubyToJS(json.value)
    return null
  }

  _getProjectInfo (project) { return this._JSONRubyToJSByCustomFieldsName(project, 'PROJECTINFO') }

  _getProjectAuditorias (project) { return this._JSONRubyToJSByCustomFieldsName(project, 'AUDITORIA') }

  _loadProjectData (project) {
    project.projectInfo = this._getProjectInfo(project)
    project.projectInfo.id = project.id
    project.projectInfo.code = project.identifier
    project.projectInfo.identifier = project.identifier
    project.projectInfo.assigned_to_id = project.assigned_to_id
    if (project.projectInfo.mapProperties) {
      project.latitude = project.projectInfo.mapProperties.mapIconPosition[0]
      project.longitude = project.projectInfo.mapProperties.mapIconPosition[1]
    }
    return project
  }

  _getProjectCustomFields (project, auditoriums, customFields) {
    return [
      {
        value: stringify(project),
        id: findIdByName(customFields, this.config.customFields.PROJECTINFO, 'CustomField')
      },
      {
        value: stringify(auditoriums),
        id: findIdByName(customFields, this.config.customFields.AUDITORIA, 'CustomField')
      }
    ]
  }
}
