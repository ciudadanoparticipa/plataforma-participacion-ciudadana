const fs = require('fs')
const path = require('path')
const chalk = require('chalk')

class HttpError extends Error {
  constructor (message, statusCode, shouldNotPrintStack = false) {
    super(message)
    this.statusCode = statusCode
    if (statusCode >= 400 && statusCode < 500) this.shouldNotPrintStack = true
    else this.shouldNotPrintStack = shouldNotPrintStack
  }
}

function clone (obj) {
  return obj ? JSON.parse(JSON.stringify(obj)) : {}
}

function formatDate (date = new Date()) {
  return `${date.getFullYear()}/${date.getMonth() + 1}/${date.getDay()} a las ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()} horas - time: ${date.getTime()}`
}

function sendError (err, res) {
  const defaultMessage = 'Por favor pongase en contacto con el administrador o equipo de desarrollo y comunique este problema.'
  if (typeof err === 'object' && err.stack) {
    if (!err.shouldNotPrintStack) {
      console.error(chalk.red(`\n[${formatDate(new Date())}] Ocurrió un error\n`))
      console.error(chalk.red(`\nStacktrace:=======================================\n${err.stack}`))
    }
    const error = clone({
      name: err.name,
      message: err.error && err.error.errors ? err.error.errors.join('\n') : `${err.message}. ${defaultMessage}`,
      error: err.error,
      statusCode: err.statusCode,
      statusMessage: err.statusMessage
    })
    if (err.statusCode && err.statusCode >= 100 && err.statusCode < 600) {
      res.status(err.statusCode).send({ error })
    } else res.status(500).send({ error: clone({ message: `${err.message}. ${defaultMessage}`, error: err.error }) })
  } else {
    console.warn(chalk.yellow('dumpError :: err is not an object'))
    console.error(chalk.red(`\n[${formatDate(new Date())}] Ocurrió un error\n`))
    console.error(chalk.red(err))
    res.status(500).send({ error: clone({ message: `${err}. ${defaultMessage}` }) })
  }
}

function handleError (responsePromise, res) {
  return responsePromise.catch(err => sendError(err, res))
}

function getFile (path) {
  try {
    return JSON.parse(fs.readFileSync(path, 'utf8'))
  } catch (e) {
    console.error(chalk.red(`\n[${formatDate(new Date())}] Error al obtener el archivo ${path}`, e.toString()), e)
    return {}
  }
}

function sendData (responsePromise, res) {
  return handleError(responsePromise, res).then(data => res.send(data))
}

function getDefaultOptions (options = {}, addApi = true) {
  const opts = {
    method: 'GET',
    headers: {
      'User-Agent': 'Request-Promise',
      'Content-Type': 'application/json'
    },
    json: true,
    ...options
  }
  if (addApi) opts.headers['X-Redmine-API-Key'] = options.apiKey
  return opts
}

function getUrl (config, resource, options = {}, { addToken = true }) {
  const { url, apiKey } = config
  const token = options.token || apiKey
  let uri = `${url}/${resource}.json`
  if (addToken) {
    uri += `?key=${token}${options.query ? '&' + options.query : ''}`
  } else {
    uri += `${options.query ? '?' + options.query : ''}`
  }
  return {
    ...options,
    uri
  }
}

function getObjectByName (arr, name) {
  if (!arr) return null
  return arr.find(el => el.name === name)
}

function getIdByName (arr, name) {
  let obj = getObjectByName(arr, name)
  return obj ? obj.id : null
}

function stringify (obj, space = 2) {
  return JSON.stringify(obj, null, space)
}

function JSONRubyToJS (obj) {
  if (obj) return JSON.parse(obj.toString())
  return null
}

function checkNotFoundError (arr, name, _typeName, findFunction) {
  let val = findFunction(arr, name)
  if (!val) {
    console.error(`${chalk.red(`${_typeName} con nombre ${name} no fue encontrado en la lista`)}: ${chalk.yellow(stringify(arr))}`)
    throw new Error(`El campo personalizado con nombre ${name} no fue encontrado en la lista o no fue creado en el REDMINE`)
  }
  return val
}

function findByName (arr, name, _typeName) {
  return checkNotFoundError(arr, name, _typeName, getObjectByName)
}

function findIdByName (arr, name, _typeName) {
  return checkNotFoundError(arr, name, _typeName, getIdByName)
}

const hasValue = obj => !!obj && !!Object.values(obj).length

const __PATH_BASE = `${path.dirname(require.main.filename)}/`

module.exports = {
  formatDate,
  getFile,
  handleError,
  sendData,
  getDefaultOptions,
  getUrl,
  stringify,
  clone,
  JSONRubyToJS,
  sendError,
  findByName,
  findIdByName,
  HttpError,
  hasValue,
  __PATH_BASE
}
