
'use strict'

const compression = require('compression')
const express = require('express')
const request = require('request-promise')
const chalk = require('chalk')
const multer = require('multer')
const { redmine } = require('./config')
const LoggedRedmineApi = require('./redmine-api/logged-api')
const PublicRedmineApi = require('./redmine-api/public-api')
const { formatDate, sendData, handleError, sendError, getDefaultOptions } = require('./utils')

redmine.apiKey = process.env.REDMINE_API_KEY || '0d76118d5af65e582df64639d36b410433285c7f'
const loggedRedmineApi = new LoggedRedmineApi(redmine, request)
const publicRedmineApi = new PublicRedmineApi(redmine, request)

const { url } = redmine
const username = process.env.REDMINE_ADMIN_USER || 'admin'
const uri = `${url}/users.json?name=${username}`
request(getDefaultOptions({ uri, apiKey: redmine.apiKey }))
  .then(data => data.users)
  .then(users => {
    if (users && users.length) return users[0]
    throw new Error(`El usuario ${username} del REDMINE no existe o ha sido bloqueado, y este es requerido para el funcionamiento del Gateway`)
  })
  .then(user => console.log(chalk.green('El API_KEY fue integrado correctamente')))
  .catch(err => {
    if (err && err.options && err.options.headers && !err.options.headers['X-Redmine-API-Key']) {
      throw new Error(`El API_KEY del REDMINE no fue seteada a la petición`)
    } else {
      throw err
    }
  })

const MAX_FILES_ALLOWED = 5

const upload = multer({ dest: `uploads/` })

const app = express()

const port = process.env.PORT || 4000

app.use(compression())

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS')
  res.header('Access-Control-Max-Age', '1728000')
  res.header('Vary', 'Accept-Encoding, Origin')
  res.header('X-Frame-Options', 'SAMEORIGIN')
  next()
})

app.use('/static', express.static(`${__dirname}/public`))

app.use(express.json())

app.get('', function (req, res) {
  res.send({ msg: 'ok' })
})

app.post('/signin', function (req, res) {
  sendData(loggedRedmineApi.signIn(req.body), res)
})

app.get('/issue/:projectId/:uuid', function (req, res) {
  sendData(publicRedmineApi.getIssueByUUID(req.params.projectId, req.params.uuid), res)
})

app.post('/issue/new', upload.array('archivos', MAX_FILES_ALLOWED), function (req, res) {
  const filesPromises = req.files.map(file => publicRedmineApi.sendFile(file))
  const files = req.files.map(file => ({ filename: file.originalname, 'content_type': file.mimetype }))
  Promise.all(filesPromises).then(values => {
    for (let i = 0; i < values.length; i++) {
      let obj = JSON.parse(values[i])
      files[i].token = obj.upload.token
    }
    sendData(publicRedmineApi.createIssue(req.body, files), res)
  }, () => handleError(filesPromises, res))
})

app.get('/projects', function (req, res) {
  sendData(publicRedmineApi.getProjects(), res)
})

app.get('/projects/:code', function (req, res) {
  sendData(publicRedmineApi.getProject(req.query.token, req.params.code), res)
})

app.put('/projects', function (req, res) {
  sendData(publicRedmineApi.updateProjects(req.body), res)
})

app.put('/project/:code', publicRedmineApi.saveAllFiles(), function (req, res) {
  sendData(publicRedmineApi.updateProject(req.query.token, JSON.parse(req.body.data), req.files), res)
})

app.post('/project/new', publicRedmineApi.saveAllFiles(), function (req, res) {
  sendData(publicRedmineApi.createProject(req.query.token, JSON.parse(req.body.data), req.files), res)
})

app.delete('/project/:code', function (req, res) {
  sendData(publicRedmineApi.deleteProject(req.query.token, req.params.code), res)
})

app.get('/projectsInfo', function (req, res) {
  publicRedmineApi.getProjects(req.query.token).then(data => res.send(data.map(project => project.projectInfo))).catch(err => {
    if (!err.response && /ECONNREFUSED/.test(err.message) && err.name === 'RequestError') {
      console.warn(chalk.red(`\n[ERROR: ${formatDate()}]`), chalk.yellow('Servidor del REDMINE no responde o esta caído'))
      res.send([])
    } else {
      sendError(err, res)
    }
  })
})

app.get('/trackers', function (req, res) {
  sendData(publicRedmineApi.getTrackers(), res)
})

app.listen(port, function () {
  console.log(chalk.green(`\nExample app listening on port ${port}!\n`))
})
