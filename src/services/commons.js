import { isIE11, isEdge, fetch } from '@/utils/browser'

export const getResponse = (promise, defaultData) => promise.then(response => {
  try {
    if ((isIE11 || isEdge) && response.response) return JSON.parse(response.response)
    if (!response.body) return defaultData || {}
    return response.json()
  } catch (e) {
    console.error(e)
    return response
  }
})

export const getData = (url, defaultData) => getResponse(fetch(url), defaultData)
