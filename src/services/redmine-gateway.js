import { stringify } from '@/utils/string-utils'
import { getResponse } from '@/services/commons'

let url = `${process.env.VUE_APP_BASE_URL}gateway/`

const options = {
  headers: {
    'Content-Type': 'application/json'
  }
}

const getProjectFormData = ({ project, files }) => {
  let formData = new FormData()
  // INICIO IMPORTANTE: No cambiar el orden, append('data', valor) DEBE HACERSE ANTES QUE LOS ARCHIVOS, o en el BACKEND VA A EXPLOTAR
  formData.append('data', stringify(project))
  // FIN    IMPORTANTE: No cambiar el orden, append('data', valor) DEBE HACERSE ANTES QUE LOS ARCHIVOS, o en el BACKEND VA A EXPLOTAR
  if (files.general) {
    Object.keys(files.general).forEach(key => {
      if (files.general[key]) Object.values(files.general[key]).forEach(file => formData.append(key, file))
    })
  }
  if (files.componentes) {
    Object.keys(files.componentes).forEach(key => {
      if (files.componentes[key]) Object.values(files.componentes[key]).forEach(file => formData.append(key, file))
    })
  }
  if (files.vistaSatelital) {
    Object.keys(files.vistaSatelital).forEach(key => {
      if (files.vistaSatelital[key]) Object.values(files.vistaSatelital[key]).forEach(file => formData.append(key, file))
    })
  }
  return formData
}

export default class RedmineService {
  static createIssue (http, data, files) {
    let formData = new FormData()
    Object.keys(data).forEach(key => formData.append(key, data[key]))
    // add files
    files.forEach(file => formData.append('archivos', file))

    // to send multipart content
    return http.post(`${url}issue/new`, formData, {
      ...options,
      headers: { 'Content-Type': 'multipart/form-data' }
    })
  }

  static getIssueByUUID (http, projectId, uuid) {
    return getResponse(http.get(`${url}issue/${projectId}/${uuid}`, options))
  }

  static getProjects (http, apiKey) {
    if (apiKey) return getResponse(http.get(`${url}projectsInfo?token=${apiKey}`, options))
    return getResponse(http.get(`${url}projectsInfo`, options))
  }

  static getProject (http, apiKey, code) {
    return http.get(`${url}projects/${code}?token=${apiKey}`, options).then(response => response.data).then(project => project.projectInfo)
  }

  static signInWithEmailAndPassword (http, username, password) {
    return http.post(`${url}signin`, { username, password }, options).then(response => response.data)
  }

  static updateProject (http, apiKey, { project, files }) {
    return http.put(`${url}project/${project.code}?token=${apiKey}`, getProjectFormData({ project, files }), {
      ...options,
      headers: { 'Content-Type': 'multipart/form-data' }
    }).then(response => response.data)
  }

  static createProject (http, apiKey, { project, files }) {
    return http.post(`${url}project/new?token=${apiKey}`, getProjectFormData({ project, files }), {
      ...options,
      headers: { 'Content-Type': 'multipart/form-data' }
    }).then(response => response.data)
  }

  static getCustomFields (http) {
    return getResponse(http.get(`${url}customFields`, options))
  }

  static getTrackers (http) {
    return getResponse(http.get(`${url}trackers`, options))
  }

  static deleteProject (http, apiKey, code) {
    return http.delete(`${url}project/${code}?token=${apiKey}`)
  }
}
