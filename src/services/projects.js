import polygonCenter from 'geojson-polygon-center'
import { projectDefaultYear, STATIC_FILES_GATEWAY_BASE_URL, projectStatus } from '@/constants'
import { fetch } from '@/utils/browser'
import { getResponse } from '@/services/commons'
import { parseNumber } from '@/utils/number'
import RedmineService from './redmine-gateway'

const getCenterLatLngByPoiner = pointer => pointer ? { lat: pointer.coordinates[1], lng: pointer.coordinates[0] } : {}

const getCenterLatLngByPolygon = polygon => getCenterLatLngByPoiner(polygonCenter(polygon))

const getMarkerPosition = (projectInfo, position) => {
  if (projectInfo) {
    if (projectInfo.number === 1) {
      position.lng -= 0.005
      position.lat -= 0.01
    }
    if (projectInfo.number === 11) {
      position.lng += 0.002
      position.lat += 0.0115
    }
    if (projectInfo.number === 5) {
      position.lng += 0.0042
    }
    if (projectInfo.number === 14) {
      position.lng += 0.0042
    }
    if (projectInfo.number === 15) {
      position.lng -= 0.0042
    }
    if (projectInfo.number === 17) {
      position.lng += 0.0010
      position.lat += 0.0010
    }
  }
  return position
}

export default class ProjectService {
  static getProjects (http, apiKey) {
    const promiseDefault = Promise.resolve([])
    try {
      return RedmineService.getProjects(http, apiKey)
        .catch(err => {
          console.error(err)
          return promiseDefault
        })
    } catch (err) {
      return promiseDefault
    }
  }

  static getProjectForFormView (http, apiKey, code) {
    return RedmineService.getProject(http, apiKey, code).then(project => {
      project.fichaTecnica = ProjectService.getFichaTecnica(project, true)
      project.componentes = ProjectService.getSubprojects(project, {}, true)
      return project
    })
  }

  static getAllExecutesInProjectAndSubProyect (project) {
    const executors = project.fichaTecnica.enteEjecutor || []
    project.componentes && project.componentes.forEach && project.componentes.forEach(subProject => {
      if (subProject.fichaTecnica && subProject.fichaTecnica.enteEjecutor) {
        executors.push(...subProject.fichaTecnica.enteEjecutor.filter(t => executors.indexOf(t) === -1))
      }
    })
    return executors
  }

  static hasExecute (projectExecute, queryExecute) {
    if (!queryExecute || !(projectExecute && projectExecute.length)) return true
    for (let execute of projectExecute) {
      if (queryExecute.indexOf(execute) >= 0) { return true }
    }
    return false
  }

  static getAllTypesInProjectAndSubProyect (project) {
    const types = project.types || []
    project.componentes && project.componentes.forEach && project.componentes.forEach(subProject => {
      if (subProject.fichaTecnica && subProject.fichaTecnica.tipo) {
        types.push(...subProject.fichaTecnica.tipo.filter(t => types.indexOf(t) === -1))
      }
    })
    return types
  }

  static hasType (projectTypes, queryTypes) {
    if (!queryTypes) return true
    for (let projectType of projectTypes) {
      if (queryTypes.indexOf(projectType) >= 0) { return true }
    }
    return false
  }

  static getCurrentState (status) {
    return ProjectService.getState(ProjectService.getStateInYear(status, projectDefaultYear))
  }

  static getStateInYear (status, year) {
    if (!status) return null
    if (status.planification > year) return null
    if (status.execution > year) return 'planification'
    if (status.finished > year) return 'execution'
    return 'finished'
  }

  static getState (keyState) {
    return projectStatus[keyState]
  }

  static hasStatusInYear (project, year, status) {
    return ProjectService.getStateInYear(project.status, year) === status
  }

  static hasAnyStatusInYear (project, year, statusList) {
    if (year && statusList) {
      for (let status of statusList) {
        if (ProjectService.hasStatusInYear(project, year, status)) return true
      }
    }
    return false
  }

  static filterProjects (proj, query) {
    const projects = Array.isArray(proj) ? proj : []

    const filterData = {
      count: {
        planification: 0,
        execution: 0,
        finished: 0
      },
      summary: {
        filter: {
          cost: 0,
          families: 0,
          projects: 0
        },
        total: {
          cost: 0,
          families: 0,
          projects: 0
        }
      }
    }

    const filteredProjects = projects.filter((project) => {
      filterData.summary.total.cost += parseNumber(project.costo)
      filterData.summary.total.families += parseNumber(project.families.amount)
      filterData.summary.total.projects++
      if (
        !ProjectService.hasExecute(this.getAllExecutesInProjectAndSubProyect(project), query.executors) ||
        !ProjectService.hasType(this.getAllTypesInProjectAndSubProyect(project), query.types) ||
        !ProjectService.hasAnyStatusInYear(project, query.year, query.status)
      ) {
        return false
      }

      const projectState = ProjectService.getStateInYear(project.status, query.year)
      if (!projectState) return false

      project.currentState = projectState
      project.fichaTecnica.estado = ProjectService.getState(project.currentState)
      filterData.count[projectState]++
      filterData.summary.filter.cost += parseNumber(project.costo)
      filterData.summary.filter.families += parseNumber(project.families.amount)
      filterData.summary.filter.projects++

      return true
    }).sort((a, b) => (a.number - b.number))

    return {
      ...filterData,
      projects: JSON.parse(JSON.stringify(filteredProjects))
    }
  }

  static addGeoData (project) {
    return getResponse(fetch(`${STATIC_FILES_GATEWAY_BASE_URL}/${project.code}/geojson/geojson.geojson`))
      .then(geoJson => {
        geoJson.features = geoJson.features.map(g => {
          if (g.properties.code) {
            g.properties = Object.assign(g.properties, project.mapProperties, { color: project.color, number: project.number })
            project.mapProperties = g.properties
          }
          return g
        })
        project.geoJson = geoJson
        return project
      })
      .catch(err => console.error(`No existe GEOJSON para el proyecto: ${project.code}`, err))
  }

  static getSubprojects (project, query, isShowInForm = false) {
    return project && project.componentes && project.componentes.filter && project.componentes.filter(subproject => subproject.name).map((subproject, index) => {
      subproject.id = index
      subproject.parentCode = project.code
      subproject.color = subproject.color || project.color
      subproject.costo = subproject.costo || project.costo
      subproject.descripcion = subproject.descripcion || project.description
      subproject.families = subproject.families || project.families
      if (!subproject.fichaTecnica) subproject.fichaTecnica = {}
      subproject.fichaTecnica.status = subproject.fichaTecnica.status || project.status
      subproject.fichaTecnica = ProjectService.getFichaTecnicaForSubproject(project, subproject, query, isShowInForm)
      subproject.slogan = subproject.slogan || project.slogan
      subproject.images = ProjectService.getImages(subproject, project.code, isShowInForm)
      subproject.documentos = ProjectService.getDocuments(subproject)
      return subproject
    })
  }

  static getSubprojectList () {
    let subprojectList = []
    return ProjectService.getProjects().then(projects => {
      projects.forEach(project => {
        if (project.componentes) {
          project.componentes.forEach(componente => {
            subprojectList.push(componente)
          })
        }
      })
      return subprojectList
    })
    // return subprojectList
  }

  static getDocuments (project) {
    return project && project.documentos ? project.documentos : []
  }

  static loadAllDocuments (project) {
    let docs = ProjectService.getDocuments(project)
    if (!docs.length && project.componentes && project.componentes.length) {
      docs = docs.concat(project.componentes
        .map(subproject => ProjectService.getDocuments(subproject))
        .reduce((before, current) => before.concat(current.filter(c => !before.includes(c))))
      )
    }
    return docs
  }

  static hasLoadImage (images) {
    return images.find(i => /\/images/.test(i))
  }

  static getImages (project, code, isShowInForm = false) {
    return project && project.images ? ProjectService.loadImages(project, code, isShowInForm).images : []
  }

  static loadImages (project, code, isShowInForm = false) {
    if (!project.loadImage && !ProjectService.hasLoadImage(project.images) && !isShowInForm) {
      project.loadImage = true
      project.images = project.images.map(src => `${STATIC_FILES_GATEWAY_BASE_URL}/${code}/images/${src}`)
    }
    return project
  }

  static loadAllImages (project) {
    let images = ProjectService.getImages(project, project.code)
    if (!images.length) {
      images = images.concat(
        project.componentes
          ? project.componentes
            .map(subproject => ProjectService.getImages(subproject, project.code))
            .reduce((before, current) => before.concat(current.filter(c => !before.includes(c)))) : []
      )
    }
    return images
  }

  static getFichaTecnica (project, isShowInForm = false) {
    const fichaTecnica = project.fichaTecnica
    fichaTecnica.tipo = ProjectService.getFichaTecnicaTipo(fichaTecnica.tipo, project.types, isShowInForm)
    fichaTecnica.enteEjecutor = ProjectService.getFichaTecnicaEnteEjecutor(fichaTecnica)
    fichaTecnica.estado = ProjectService.getState(project.currentState)
    fichaTecnica.superficie = ProjectService.getFichaTecnicaSuperficie(fichaTecnica)
    fichaTecnica.familias = ProjectService.getFichaTecnicaFamilias(fichaTecnica, project)
    return fichaTecnica
  }

  static getFichaTecnicaForSubproject (project, subproject, query, isShowInForm) {
    const fichaTecnica = Object.assign({}, subproject.fichaTecnica || project.fichaTecnica)
    fichaTecnica.tipo = ProjectService.getFichaTecnicaTipo(fichaTecnica.tipo, project.types, isShowInForm)
    fichaTecnica.enteEjecutor = ProjectService.getFichaTecnicaEnteEjecutor(fichaTecnica)
    fichaTecnica.estado = ProjectService.getState(ProjectService.getStateInYear(subproject.fichaTecnica.status, query.year)) || ProjectService.getState(project.currentState)
    fichaTecnica.superficie = ProjectService.getFichaTecnicaSuperficie(fichaTecnica)
    fichaTecnica.familias = ProjectService.getFichaTecnicaFamilias(fichaTecnica, subproject)
    return fichaTecnica
  }

  static getFichaTecnicaTipo (tipo, projectTypes, isShowInForm) {
    if (isShowInForm) {
      if (typeof tipo === 'string') {
        return tipo.replace('Vial - Movilidad', 'Vial * Movilidad').replace('Vial - Ordenamiento urbano', 'Vial * Ordenamiento urbano').split(' - ').map(s => s.replace(/\*/g, '-'))
      }
      return tipo || projectTypes
    } else {
      return (tipo && tipo.join && tipo.join(' - ')) || projectTypes.join(' - ')
    }
  }

  static getFichaTecnicaEnteEjecutor (fichaTecnica) {
    return fichaTecnica.enteEjecutor
  }

  static getFichaTecnicaSuperficie (fichaTecnica) {
    return fichaTecnica.superficie && (fichaTecnica.superficie + (fichaTecnica.superficie.match && fichaTecnica.superficie.match(/[a-z]/i) ? '' : ' Has'))
  }

  static getFichaTecnicaFamilias (fichaTecnica, project) {
    return fichaTecnica.familias ? fichaTecnica.familias : project.families.message ? project.families.message : project.families.amount ? project.families.amount.toLocaleString('es') + ' familias residentes en el área.' : 'Sin familias residentes en el área.'
  }

  static getMarkerPositionByLatLng (projectInfo, latlng) {
    return getMarkerPosition(projectInfo, latlng)
  }

  static getMarkerPositionByGeoJson (projectInfo, geoJson) {
    return getMarkerPosition(projectInfo, getCenterLatLngByPolygon(geoJson.features.find(f => f.properties.parent)))
  }

  static getNewNumber (http) {
    return RedmineService.getProjects(http).then(projects => projects.length + 1)
  }

  static deleteProject (http, apiKey, code) {
    return RedmineService.deleteProject(http, apiKey, code)
  }
}
