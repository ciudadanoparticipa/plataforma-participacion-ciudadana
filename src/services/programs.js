export default class ProgramService {
  static getPrograms () {
    return require('../assets/json/programs.json')
  }
}
