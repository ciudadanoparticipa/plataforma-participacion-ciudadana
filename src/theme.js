export const grayColor = '#888'

export default {
  primary: '#0d2563',
  accent: '',
  pie: {
    primary: grayColor,
    secondary: '#b8b8b8',
    tertiary: '#cfcfcf'
  },
  defaultHeaderColor: '#0d2563',
  styles: {
    cotas: {
      color: 'rgb(51, 136, 255)',
      fillColor: 'rgb(51, 136, 255)',
      fillOpacity: 0.6,
      opacity: 1,
      stroke: 'rgb(51, 136, 255)',
      weight: 2
    }
  }
}
