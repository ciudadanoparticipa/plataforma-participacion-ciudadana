/**
 *  Este archivo se usa para definir watchers globales entre stores, es decir, sirve para vincular un store con otro store
 */
export default ({ $store }) => {
  /* Watch entre refugios-filtro y refugios
   * Si cambia algún valor en los filtros de refugios entonces se cambia los refugios a mostrarse según el filtro
   */
  $store.watch(
    rootState => rootState.projectsFilter.filters.projects,
    () => $store.dispatch('filterProjects')
  )

  /* Watch entre refugios-filtro y refugios
   * Si cambia algún valor en los filtros de refugios entonces se cambia los refugios a mostrarse según el filtro
   */
  $store.watch(
    rootState => rootState.refugiosFilter.filters.refugios,
    () => $store.dispatch('loadRefugios', $store.state.refugios.refugiosYear)
  )

  /* Watch entre instituciones-filtro e instituciones
   * Si cambia algún valor en los filtros de instituciones entonces se cambia los instituciones a mostrarse según el filtro
   */
  $store.watch(
    rootState => rootState.institucionesFilter.filters.instituciones,
    () => {
      $store.dispatch('loadCentrosMunicipales')
      $store.dispatch('loadInstituciones')
    }
  )

  /* Watch entre controles y refugios
   * Si cambia el control de refugio entonces se muestra o se borra los refugios del mapa
   */
  $store.watch(
    rootState => rootState.controls.interface.state.refugios,
    val => $store.dispatch('activeOrDeactiveRefugios', val)
  )

  /* Watch entre controles e instituciones
   * Si cambia el control de instituciones entonces se muestra o se borra los refugios del mapa
   */
  $store.watch(
    rootState => rootState.controls.interface.state.instituciones,
    val => $store.dispatch('activeOrDeactiveInstituciones', val)
  )

  /* Watch entre projectInfo seleccionado y refugios e instituciones seleccionadas
   * Si cambia el proyecto seleccionado entonces se debe nular ya sea el refugio o la instituciones seleccionadas
   */
  $store.watch(
    rootState => rootState.projects.projectInfo,
    val => {
      if (val) {
        $store.dispatch('setInstitucion', null)
        $store.dispatch('setCentroMunicipal', null)
        $store.dispatch('setCurrentRefugio', null)
        if (!val.componentes) {
          $store.dispatch('setActiveTab', 'descripcion')
        }
      }
    }
  )

  /* Watch entre refugio seleccionado y proyectos e instituciones seleccionadas
   * Si cambia el refugio seleccionado entonces se debe nular ya sea el proyecto o la instituciones seleccionadas
   */
  $store.watch(
    rootState => rootState.refugios.currentRefugio,
    val => {
      if (val) {
        $store.dispatch('setInstitucion', null)
        $store.dispatch('setCentroMunicipal', null)
        $store.dispatch('showProjectInfo', null)
      }
    }
  )

  /* Watch entre centroMunicipal seleccionado y refugios, instituciones y proyectos seleccionados
   * Si cambia el centroMunicipal seleccionado entonces se debe nular ya sea el refugio, la institución o el proyecto seleccionado
   */
  $store.watch(
    rootState => rootState.instituciones.centroMunicipal.current,
    val => {
      if (val) {
        $store.dispatch('setInstitucion', null)
        $store.dispatch('showProjectInfo', null)
        $store.dispatch('setCurrentRefugio', null)
      }
    }
  )

  /* Watch entre institucion seleccionada y refugios, centroMunicipales y proyectos seleccionados
   * Si cambia la institución seleccionada entonces se debe nular ya sea el refugio, el centroMunicipal o el proyecto seleccionado
   */
  $store.watch(
    rootState => rootState.instituciones.institucion.current,
    val => {
      if (val) {
        $store.dispatch('showProjectInfo', null)
        $store.dispatch('setCentroMunicipal', null)
        $store.dispatch('setCurrentRefugio', null)
      }
    }
  )

  $store.watch(
    rootState => rootState.route.query,
    val => {
      if (val.refugio_nro) {
        $store.dispatch('activateControl', 'refugios')
      } else if (val.institucion_id || val.centro_municipal_id) {
        $store.dispatch('activateControl', 'instituciones')
      }
    }
  )

  $store.watch(
    rootState => rootState.auth.message,
    () => $store.dispatch('updateSessionInLocalStorage')
  )

  $store.watch(
    rootState => rootState.auth.user,
    (user, oldUser) => {
      $store.dispatch('updateSessionInLocalStorage')
      if (!user && oldUser && oldUser.api_key) $store.dispatch('redirectToLogin')
    }
  )
}
