export default ({ $store, $http }) => {
  // load projects
  $store.dispatch('loadProjectDataAndFilter', $http)
  // load redmine data. Load the options needed to build an issue
  $store.dispatch('loadTrackers', $http)
  // load refugios
  $store.dispatch('loadAllDataInRefugios', $store.state.refugios.refugiosYear)
  // load instituciones
  $store.dispatch('loadAllDataInInstituciones')
}
