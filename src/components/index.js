import Vue from 'vue'
import Footer from '@/components/Footer'
import AsuHeader from '@/components/AsuHeader'
import ControlPanel from '@/components/controls/ControlPanel'
import TicketInfo from '@/components/utils/TicketInfo'
import ColorPicker from '@/components/utils/ColorPicker'
import ErrorDialog from '@/components/utils/ErrorDialog'
import ButtonGroup from '@/components/utils/ButtonGroup'
import DownloadButton from '@/components/utils/DownloadButton'
import ButtonWithAlertDialog from '@/components/utils/ButtonWithAlertDialog'
import TextBoxEditor from '@/components/utils/TextBoxEditor'
import TwoColumnDataPanel from '@/components/utils/TwoColumnDataPanel'
import CityBackgroundImage from '@/components/utils/CityBackgroundImage'
import CustomInputFile from '@/components/utils/CustomInputFile'
import CustomSelectGroup from '@/components/utils/CustomSelectGroup'
import CustomDialogConfirm from '@/components/utils/CustomDialogConfirm'
import LineMultiColorHeaderSeparator from '@/components/utils/LineMultiColorHeaderSeparator'

Vue.component('Footer', Footer)
Vue.component('AsuHeader', AsuHeader)
Vue.component('TicketInfo', TicketInfo)
Vue.component('ColorPicker', ColorPicker)
Vue.component('ErrorDialog', ErrorDialog)
Vue.component(ButtonGroup.name, ButtonGroup)
Vue.component(DownloadButton.name, DownloadButton)
Vue.component('ButtonWithAlertDialog', ButtonWithAlertDialog)
Vue.component('TextBoxEditor', TextBoxEditor)
Vue.component('TwoColumnDataPanel', TwoColumnDataPanel)
Vue.component('ControlPanel', ControlPanel)
Vue.component('CityBackgroundImage', CityBackgroundImage)
Vue.component('CustomInputFile', CustomInputFile)
Vue.component('CustomSelectGroup', CustomSelectGroup)
Vue.component('CustomDialogConfirm', CustomDialogConfirm)
Vue.component('LineMultiColorHeaderSeparator', LineMultiColorHeaderSeparator)
