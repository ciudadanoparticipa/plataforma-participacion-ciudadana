import Vue from 'vue'
import Vuetify from 'vuetify'
import theme from '@/theme.js'

Vue.use(Vuetify, {
  theme: {
    primary: theme.primary
  }
})
