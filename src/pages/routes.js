import Home from '@/pages/Home.vue'
import AsuParticipa from '@/pages/asu/Participa.vue'
import AsuCostera from '@/pages/asu/Costera.vue'
import AsuDownload from '@/pages/asu/AsuDownload.vue'
import AsuViva from '@/pages/asu/Viva.vue'
import Map from '@/pages/Map.vue'
import RedmineIssueMap from '@/pages/RedmineIssueMap.vue'
import SeguimientoReclamo from '@/pages/SeguimientoReclamo.vue'
import Projects from '@/pages/Projects.vue'
import ParticipaWidget from '@/pages/ParticipaWidget.vue'
import ListProject from '@/pages/ListProject.vue'
import ProjectFrom from '@/pages/ProjectFrom.vue'
import Login from '@/pages/Login.vue'

export default [
  { path: '/', component: Home, name: 'home' },
  { path: '/enviar-mensaje/:tipo', component: Home, name: 'homesendmsg', props: { openOnLoad: 'enviarMensaje' } },
  { path: '/participacion-ayuda', component: Home, name: 'homehelp', props: { openOnLoad: 'ayuda' } },
  { path: '/asu/viva', component: AsuViva, name: 'asuviva', meta: { page: 'viva' } },
  { path: '/asu/participa', component: AsuParticipa, name: 'asuparticipa', meta: { page: 'participa' } },
  { path: '/asu/costera', component: AsuCostera, name: 'asucostera', meta: { page: 'costera' } },
  { path: '/asu/:page/download', component: AsuDownload, name: 'asudownload', props: true },
  { path: '/map', component: Map, name: 'map' },
  { path: '/projects', component: Projects, name: 'projects' },
  { path: '/redmine-issue-map', component: RedmineIssueMap, name: 'redmine-issue-map', meta: { isRedmineMap: true } },
  { path: '/seguimiento/reclamos/:projectId/:uuid', component: SeguimientoReclamo, name: 'seguimiento-reclamo', meta: { hideMenuMobile: true } },
  { path: '/participa-widget', component: ParticipaWidget, name: 'participaWidget', meta: { embedded: true }, query: { institucion: '' } },
  { path: '/project/list', component: ListProject, name: 'listProject', meta: { requiresAuth: true } },
  { path: '/project/edit/', redirect: { name: 'listProject' }, meta: { requiresAuth: true } },
  { path: '/project/edit/:code', component: ProjectFrom, name: 'editProject', meta: { requiresAuth: true } },
  { path: '/project/create', component: ProjectFrom, name: 'createProject', meta: { requiresAuth: true, isCreate: true } },
  { path: '/login', component: Login, name: 'login' }
]
