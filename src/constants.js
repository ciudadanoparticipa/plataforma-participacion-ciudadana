import { isIE11, isEdge } from '@/utils/browser'

let STATIC_FILES_GATEWAY_PROXY_BASE_URL = `${process.env.VUE_APP_GATEWAY_BASE_URL}static/projects`
if (isIE11 || isEdge) STATIC_FILES_GATEWAY_PROXY_BASE_URL = `${process.env.VUE_APP_BASE_URL}gateway/static/projects`

export const centroMunicipalURL = 'http://www.asuncion.gov.py/centros-municipales'

export const tooltipOptions = {
  direction: 'top'
}

export const monthMapper = {
  'Ene': 'Enero',
  'Feb': 'Febrero',
  'Mar': 'Marzo',
  'Abr': 'Abril',
  'May': 'Mayo',
  'Jun': 'Junio',
  'Jul': 'Julio',
  'Ago': 'Agosto',
  'Set': 'Setiembre',
  'Oct': 'Octubre',
  'Nov': 'Noviembre',
  'Dic': 'Diciembre'
}

export const projectDefaultYear = (new Date()).getFullYear()

export const projectStatus = {
  planification: 'Planificación',
  execution: 'Ejecución',
  finished: 'Finalizado'
}

export const projectTypes = {
  urbanizacionPrivada: 'Urbanización privada',
  urbanizacionSocial: 'Urbanización social',
  consolidacionBarrios: 'Consolidación de barrios',
  vialOrdenamientoUrbano: 'Vial - Ordenamiento urbano',
  vialMovilidad: 'Vial - Movilidad',
  espacioPublico: 'Espacio Público',
  oficinasGobierno: 'Oficinas de Gobierno',
  recuperacionAmbiental: 'Recuperación Ambiental',
  infraestructura: 'Infraestructura'
}

export const projectExecutors = {
  mac: 'Municipalidad de Asunción',
  itaipu: 'Itaipú',
  mopc: 'Ministerio de Obras Públicas y Comunicaciones (MOPC)',
  app: 'Asociación Público Privada (APP)',
  muvh: 'Ministerio de Urbanismo, Vivienda y Hábitat (MUVH)',
  essap: 'Empresa de Servicios Sanitarios del Paraguay S.A. (ESSAP)'
}

export const projectFilterObject = {
  status: Object.entries(projectStatus).map(([ value, text ]) => ({ value, text })),
  types: Object.values(projectTypes).map(text => ({ text })),
  executors: Object.values(projectExecutors).map(text => ({ text }))
}

export const estadoRefugios = {
  censado: 'Censado',
  noCensado: 'No Censado',
  noHabitable: 'No Habitable'
}

export const zonaRefugios = {
  sur: 'Sur',
  norte: 'Norte',
  chacarita: 'Chacarita',
  noDefinida: 'No definida'
}

export const composicionFamiliarRefugios = {
  ninhos: 'Niños',
  adultos: 'Adultos',
  adolecentes: 'Adolecentes',
  terceraEdad: 'Tercera edad',
  personasDiscapacitadas: 'Personas con discapacidades'
}

export const barriosRefugios = {
  bancoSanMiguel: 'Banco San Miguel',
  banadoCaraCara: 'Bañado Cara Cara',
  bellaVista: 'Bella Vista',
  botanico: 'Botánico',
  drGasparRodriguezDeFrancia: 'Dr. Gaspar Rodriguez De Francia',
  itaEnramada: 'Ita Enramada',
  itaPytaPunta: 'Ita Pyta Punta',
  jara: 'Jara',
  jukyty: 'Jukyty',
  laCatedral: 'La Catedral',
  lasMercedes: 'Las Mercedes',
  obreroIntendenteBGuggiari: 'Obrero Intendente B. Guggiari',
  presidenteCarlosAntonioLopez: 'Presidente Carlos Antonio López',
  republicano: 'Republicano',
  ricardoBrugada: 'Ricardo Brugada',
  robertoLPetit: 'Roberto L. Petit',
  sajonia: 'Sajonia',
  sanAntonio: 'San Antonio',
  sanCayetano: 'San Cayetano',
  santaAna: 'Santa Ana',
  santaLibrada: 'Santa Librada',
  santaRosa: 'Santa Rosa',
  santisimaTrinidad: 'Santísima Trinidad',
  tabladaNueva: 'Tablada Nueva',
  tacumbu: 'Tacumbú',
  virgenDeFatima: 'Virgen De Fátima',
  virgenDeLaAsuncion: 'Virgen De La Asunción',
  zeballosKue: 'Zeballos Kue',
  noDefinido: 'No definido'
}

export const emptyGeoJson = { features: [] }

export const refugioDefaultYear = 2018

export const refugioFilterObject = {
  refugiosStatus: Object.values(estadoRefugios).filter(s => s !== estadoRefugios.noHabitable),
  zonas: Object.values(zonaRefugios),
  composicionFamiliar: Object.values(composicionFamiliarRefugios),
  barrios: Object.values(barriosRefugios)
}

export const mapBetweenComposicionFamiliarAndCensoKeys = {
  [composicionFamiliarRefugios.adultos]: 'adultos',
  [composicionFamiliarRefugios.adolecentes]: 'adolecentes',
  [composicionFamiliarRefugios.ninhos]: 'totalNiños',
  [composicionFamiliarRefugios.terceraEdad]: 'adultosMayores',
  [composicionFamiliarRefugios.personasDiscapacitadas]: 'discapacidad'
}

export const PROJECT_FILTER_MODULE_NAME = 'projects'
export const REFUGIO_FILTER_MODULE_NAME = 'refugios'
export const INSTITUCIONES_FILTER_MODULE_NAME = 'instituciones'

export const fuenteRefugioCensoByYear = {
  '2014': `<strong>Fuente:</strong> Fichas censales que corresponden a las atenciones realizadas durante el año 2014 de las familias afectadas por la inundación. (Fichas censales correspondientes al año 2014)`,
  '2015': `<strong>Fuente:</strong> Fichas censales que corresponden a las atenciones realizadas durante el año 2015 de las familias afectadas por la inundación. (Fichas censales correspondientes al año 2015)`,
  '2016': `<strong>Fuente:</strong> Fichas censales que corresponden a las atenciones realizadas durante el año 2016 de las familias afectadas por la inundación. (Fichas censales correspondientes al año 2016)`,
  '2017': `<strong>Fuente:</strong> Fichas censales que corresponden a las atenciones realizadas durante el año 2017 de las familias afectadas por la inundación. (Fichas censales correspondientes al año 2017)`,
  '2018': `<strong>Fuente:</strong> Censo realizado en conjunto por la Cruz Roja Paraguaya y la Municipalidad de Asunción, actualizado a: 20-12-2018.`,
  'updatedAt': `<strong>Fuente:</strong> Censo realizado en conjunto por la Cruz Roja Paraguaya y la Municipalidad de Asunción, actualizado a:`
}

export const institucionesTypes = {
  escuelas: 'Instituciones Educativas',
  hospitales: 'Instituciones de Salud',
  iglesias: 'Instituciones Religiosas',
  centrosMunicipales: 'Centros Municipales',
  comisarias: 'Comisarias'
}

export const institucionesFilterObject = {
  institucionesTypes: Object.values(institucionesTypes)
}

export const cotaNumberList = [ 58, 59, 60, 61, 62, 63 ]

export const cotaByMtsObj = {
  '58': '4mts',
  '59': '5mts',
  '60': '6mts',
  '61': '7mts',
  '62': '8mts',
  '63': '9mts'
}

export const initialCota = cotaNumberList[0]

export const monedas = [ 'US$', 'Gs.', '€' ]

export const defaultMessageError = 'Por favor pongase en contacto con el administrador o equipo de desarrollo y comunique este problema.'

export const STATIC_FILES_GATEWAY_BASE_URL = STATIC_FILES_GATEWAY_PROXY_BASE_URL
