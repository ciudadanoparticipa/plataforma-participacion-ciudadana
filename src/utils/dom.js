export const findElementInHtmlListByIdEndsWith = (list, nro) => list.find(r => r.id.endsWith(nro))
export const findIndexElementInHtmlListByIdEndsWith = (list, nro) => list.findIndex(r => r.id.endsWith(nro))

export const scrollElement = (element, parent) => {
  if (element) {
    const scrollTo = element.getBoundingClientRect().top - parent.getBoundingClientRect().top
    try {
      parent.scrollTo(0, scrollTo)
    } catch (e) {
      parent.scrollTop = scrollTo
    }
  }
}

export const scrollElementIfExistInList = (list, elementId, parent) => {
  if (list) {
    parent.scrollTop = 0
    scrollElement(findElementInHtmlListByIdEndsWith(list, elementId), parent)
  }
}
