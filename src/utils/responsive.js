const doc = window.document
const docEl = doc.documentElement

export const isLandscape = () => window.innerWidth < window.innerHeight
export const isMobile = () => window.innerWidth < 851
export const hasWidthGt1920 = () => window.innerWidth > 1920
export const resizeRootFont = () => {
  const clientWidth = docEl.clientWidth && docEl.clientWidth > 1600 ? docEl.clientWidth : 1600
  const fontSize = clientWidth / 100
  doc.body.style.fontSize = fontSize + 'px'
  docEl.style.fontSize = fontSize + 'px'
  docEl.style.display = 'none'
  // docEl.clientWidth // Force relayout - important to new Androids
  docEl.style.display = ''
}
export const addListenerForResizeRoot = () => {
  // Abort if browser does not support addEventListener
  if (!doc.addEventListener) return

  // Test rem support
  const div = doc.createElement('div')
  div.setAttribute('style', 'font-size: 1rem')

  // Abort if browser does not recognize rem
  if (div.style.fontSize !== '1rem') return

  window.addEventListener('resize', resizeRootFont, false)
  doc.addEventListener('DOMContentLoaded', resizeRootFont, false)
}
