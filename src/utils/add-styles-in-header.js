export const separatorColors = [
  '#ffd20a',
  '#da7127',
  '#0ad2ff',
  '#4d3b89',
  '#00a64f',
  '#ffd20a',
  '#da7127',
  '#0ad2ff',
  '#4d3b89',
  '#00a64f'
]

export const addRules = (selector, css) => {
  try {
    const styleEl = document.createElement('style')
    styleEl.innerHTML = ''
    styleEl.class = 'dynamicStyle'
    const propText = typeof css === 'string' ? css : Object.keys(css).map(p => (p + ':' + (p === 'content' ? "'" + css[p] + "'" : css[p]))).join(';')
    styleEl.innerHTML += selector + '{' + propText + '}'
    // Aparentemente ¿alguna versión de Safari necesita la siguiente linea? No lo sé.
    styleEl.appendChild(document.createTextNode(''))
    document.head.appendChild(styleEl)
  } catch (e) {
    console.error('Error al insertar estilos dinamicamente', e)
  }
}
