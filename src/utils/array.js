export const clone = arr => arr ? JSON.parse(JSON.stringify(arr)) : []
export const hasValue = arr => arr && arr.length
export const getArray = arr => hasValue(arr) ? arr : []
export const existInArray = (arr, val, findFunction) => findFunction ? arr.find(findFunction) : arr.indexOf(val) > -1
export const pushIfNotExist = (arr, val, findFunction) => {
  if (existInArray(arr, val, findFunction)) return
  arr.push(val)
}
export const clean = arr => {
  if (!hasValue(arr)) return []
  return arr.splice(0, arr.length)
}

export const arrayDiff = (a1, a2) => {
  const a = []
  const diff = []

  for (let i = 0, len = a1.length; i < len; i++) {
    a[a1[i]] = true
  }

  for (let i = 0, len = a2.length; i < len; i++) {
    if (a[a2[i]]) {
      delete a[a2[i]]
    } else {
      a[a2[i]] = true
    }
  }

  for (let k in a) {
    diff.push(k)
  }

  return diff
}
