import Vue from 'vue'
import 'material-design-icons-iconfont/dist/material-design-icons.css'

import {
  MdApp,
  MdButton,
  MdCheckbox,
  MdContent,
  MdDialog,
  MdDialogConfirm,
  MdDivider,
  MdDrawer,
  MdEmptyState,
  MdIcon,
  MdList,
  MdMenu,
  MdRadio,
  MdSubheader,
  MdTable,
  MdTabs,
  MdToolbar,
  MdTooltip,
  MdCard,
  MdField,
  MdProgress,
  MdRipple,
  MdSnackbar,
  MdSpeedDial
} from 'vue-material/dist/components'

Vue.use(MdApp)
Vue.use(MdCard)
Vue.use(MdButton)
Vue.use(MdContent)
Vue.use(MdCheckbox)
Vue.use(MdDivider)
Vue.use(MdDrawer)
Vue.use(MdEmptyState)
Vue.use(MdIcon)
Vue.use(MdList)
Vue.use(MdMenu)
Vue.use(MdRadio)
Vue.use(MdSubheader)
Vue.use(MdTable)
Vue.use(MdTabs)
Vue.use(MdToolbar)
Vue.use(MdTooltip)
Vue.use(MdDialog)
Vue.use(MdDialogConfirm)
Vue.use(MdSpeedDial)
Vue.use(MdField)
Vue.use(MdProgress)
Vue.use(MdRipple)
Vue.use(MdRadio)
Vue.use(MdProgress)
Vue.use(MdSnackbar)
Vue.use(MdMenu)
