import colorPropertyMixin from './color-property.js'
import tabsMixin from './tabs.js'
import { addRules } from '@/utils/add-styles-in-header.js'

export default {
  mixins: [colorPropertyMixin, tabsMixin],
  methods: {
    addRulesForTabIndicator () {
      addRules('.leaflet-touch .leaflet-sidebar.right > .leaflet-control', {
        'border-bottom-color': this.color,
        'border-left-color': this.color
      })
    },
    getTitleClass (name) {
      return name.length > 20 ? 'gt20' : ''
    }
  }
}
