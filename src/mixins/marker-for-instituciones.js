import L from 'leaflet'
import { tooltipOptions } from '@/constants.js'
import markerMixin from '@/mixins/marker.js'

export default {
  mixins: [ markerMixin ],
  data () {
    const self = this
    return {
      options: {
        filter: (feature, layer) => feature.geometry,
        onEachFeature (feature, layer) {
          layer.bindTooltip(self.buildPopup(feature.properties), tooltipOptions)
        },
        pointToLayer (geoJsonPoint, latlng) {
          const marker = L.marker(latlng, { icon: self.buildIcon(geoJsonPoint.properties) })
            .on('click', event => self.showDetalle(geoJsonPoint.properties, event))
          self.setMarkerObject(marker, geoJsonPoint.properties.id)
          return marker
        }
      }
    }
  }
}
