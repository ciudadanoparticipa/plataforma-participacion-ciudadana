export default {
  components: {
    GoBackButton: _ => import('@/components/utils/GoBackButton')
  },
  methods: {
    goToHome () {
      this.$router.push('/')
    },
    goToBack () {
      window.history.length > 1 ? this.$router.go(-1) : this.$router.push('/')
    }
  }
}
