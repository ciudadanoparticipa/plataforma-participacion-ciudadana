import { mapGetters } from 'vuex'
import { validationMixin } from 'vuelidate'

const getNestedObject = (nestedObj, pathToFieldName) => {
  return pathToFieldName.split(/\.|\//g).reduce((obj, key) => obj ? obj[key] : undefined, nestedObj)
}

export default {
  mixins: [validationMixin],
  methods: {
    getValidationClass (pathToFieldName) {
      const field = getNestedObject(this.$v, pathToFieldName)
      if (field) {
        return {
          'md-invalid': field.$invalid && field.$dirty
        }
      }
    },
    getMessageErrors (pathToFieldName) {
      if (!this.$v || !this.$v.$anyDirty) return []
      const field = getNestedObject(this.$v, pathToFieldName)
      if (!field || !field.$dirty) return []
      const validators = Object.keys(field.$params)
      const errors = []
      validators.forEach(validator => {
        if (!field[validator]) {
          if (validator === 'required') errors.push('Este campo es requerido')
          if (validator === 'maxLength') errors.push(`Este campo debe tener hasta ${field.$params.maxLength.max} caracteres`)
          if (validator === 'minLength') errors.push(`Este campo debe tener al menos ${field.$params.minLength.min} caracteres`)
          if (validator === 'maxValue') errors.push(`El valor debe ser menor que ${field.$params.maxValue.max} caracteres`)
          if (validator === 'minValue') errors.push(`El valor debe ser mayor que ${field.$params.minValue.max} caracteres`)
          if (validator === 'between') errors.push(`El valor debe estar entre ${field.$params.between.max} y ${field.$params.between.max}`)
          if (validator === 'email') errors.push(`El correo electrónico cargado es inválido`)
          if (validator === 'alpha') errors.push(`El valor es inválido, solo se aceptan letras`)
          if (validator === 'alphaNum') errors.push(`El valor es inválido, solo se aceptan números o letras`)
          if (validator === 'numeric') errors.push(`El número introducido es inválido`)
          if (validator === 'integer') errors.push(`El número introducido es inválido, solo se aceptan enteros positivos o negativos`)
          if (validator === 'decimal') errors.push(`El número introducido es inválido, solo se aceptan decimales positivos o negativos`)
          if (validator === 'ipAddress') errors.push(`La IP es inválida, solo se aceptan IPv4, ejemplo: 127.0.0.1`)
          if (validator === 'url') errors.push(`La URL es inválida`)
        }
      })
      return errors
    },
    isValid () {
      this.$v.$touch()
      return !this.$v.$invalid
    }
  },
  computed: {
    ...mapGetters({
      'sending': 'httpIsProcessing'
    })
  }
}
