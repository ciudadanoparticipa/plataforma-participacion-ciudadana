import { mapGetters } from 'vuex'

export default {
  computed: {
    ...mapGetters(['isCurrentRoute']),
    isInRoot () {
      return this.isCurrentRoute({ name: 'home' })
    }
  }
}
