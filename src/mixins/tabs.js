export default {
  data: () => ({
    activeTab: 'tab-1'
  }),
  methods: {
    onChangeTabs (tab) {
      this.activeTab = tab
    }
  }
}
