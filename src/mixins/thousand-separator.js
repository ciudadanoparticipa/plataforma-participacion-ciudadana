import { inputValueThousandSeparatorFormat } from '@/utils/number.js'

export default {
  methods: {
    inputValueThousandSeparatorFormat (event, callbackToUpdateVModel = () => null) {
      callbackToUpdateVModel(inputValueThousandSeparatorFormat(event))
    }
  }
}
