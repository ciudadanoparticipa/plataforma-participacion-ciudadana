import { clone } from '@/utils/form.js'
import { STATIC_FILES_GATEWAY_BASE_URL } from '@/constants.js'
import vModelWithDeepWatchMixin from '@/mixins/v-model-with-deep-watch.js'

export default {
  mixins: [vModelWithDeepWatchMixin],
  props: {
    code: {
      type: String,
      default: undefined
    },
    isCreateView: {
      type: Boolean,
      default: false
    },
    customValidations: {
      type: Object,
      default: () => ({})
    },
    buttonNextPressed: {
      type: Boolean,
      default: false
    }
  },
  data: () => ({
    firstChange: true,
    formData: null
  }),
  methods: {
    submit (event, tab, forcePassValidation = false) {
      event.preventDefault()
      this.$emit('on-save-press', {
        tab,
        isValid: forcePassValidation || this.isValid(),
        data: this.value
      })
    },
    getImageSRC (imgFolder, imgName) {
      return `${STATIC_FILES_GATEWAY_BASE_URL}/${this.code}/${imgFolder}/${imgName}`
    },
    initDataWhenValuePropIsAssignedFirstTime (value) { }
  },
  watch: {
    value: {
      handler (newVal, oldVal) {
        if (this.firstChange && !oldVal && newVal) {
          this.initDataWhenValuePropIsAssignedFirstTime(newVal)
          this.formData = clone(newVal)
          this.firstChange = false
        }
      },
      immediate: true,
      deep: true
    },
    formData: {
      handler (newVal, oldVal) {
        if (oldVal) this.$emit('input', newVal)
      },
      deep: true
    },
    buttonNextPressed (val) {
      if (val) this.$refs.saveButton.$el.click()
    }
  }
}
