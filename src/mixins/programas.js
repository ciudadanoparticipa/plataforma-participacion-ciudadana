import { mapGetters, mapActions } from 'vuex'

export default {
  methods: {
    ...mapActions(['showPrograms'])
  },
  computed: {
    ...mapGetters(['programsState'])
  }
}
