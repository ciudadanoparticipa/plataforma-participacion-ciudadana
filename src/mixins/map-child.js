import { findRealParent } from 'vue2-leaflet'
import { getData } from '@/utils/object.js'

export default {
  computed: {
    lmapObject () {
      try {
        return getData(findRealParent(this.$parent, true)).mapObject
      } catch (e) {
        return undefined
      }
    }
  }
}
