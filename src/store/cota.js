import { initialCota } from '@/constants'
import CotaService from '@/services/cota'

export default {
  state: {
    currentCota: initialCota,
    hideFloodZones: false
  },
  mutations: {
    setCurrentCota: (state, cota) => { state.currentCota = cota },
    setHideFloodZones: (state, val) => { state.hideFloodZones = val }
  },
  actions: {
    setCurrentCota: ({ commit, dispatch }, cota) => {
      commit('setCurrentCota', cota)
      dispatch('setHideFloodZones', !cota)
    },
    setHideFloodZones: ({ commit }, val) => commit('setHideFloodZones', val)
  },
  getters: {
    currentCota: state => state.currentCota,
    floodZones: state => CotaService.getZonasInundables(state.currentCota),
    affectedInstitutions: state => CotaService.getInstitucionesAfectadas(state.currentCota),
    hideFloodZones: state => state.hideFloodZones
  }
}
