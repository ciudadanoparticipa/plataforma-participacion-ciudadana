import { clone } from '@/utils/object'

const initialState = {
  families: true,
  movileView: true,
  refugioPanel: false,
  labels: false,
  shapes: true,
  refugios: false,
  instituciones: false
}

const controlWithUIPanel = {
  filter: false,
  controls: false,
  projectList: false
}

const visibilityOfControls = {
  filter: true,
  controls: true,
  projectList: true,

  families: true,
  movileView: true,
  refugioPanel: false,
  labels: true,
  shapes: true,
  refugios: false,
  instituciones: false
}

export default {
  state: {

    interface: {
      state: { ...initialState },
      visibility: { ...visibilityOfControls },
      controlWithUIPanel: { ...controlWithUIPanel },
      hoverDataEvent: null
    },

    controlUIPanel: false

  },

  mutations: {

    hoverProject: (store, hoverDataEvent) => {
      store.interface = {
        ...store.interface,
        hoverDataEvent
      }
    },

    unHoverProject: (store) => {
      store.interface = {
        ...store.interface,
        hoverDataEvent: null
      }
    },

    setControlStatus: (store, { name, value }) => {
      store.interface.state = {
        ...store.interface.state,
        [name]: value
      }
    },

    setControlWithUIPanel: (store, { name, value }) => {
      store.interface.controlWithUIPanel = {
        ...store.interface.controlWithUIPanel,
        [name]: value
      }
    },

    resetControlPanelStatusExceptBy: (store, name) => {
      let initialControlPanelStatus = clone(controlWithUIPanel)
      if (name) delete initialControlPanelStatus[name]
      store.interface.controlWithUIPanel = {
        ...store.interface.controlWithUIPanel,
        ...initialControlPanelStatus
      }
    },

    resetInterface: (store) => {
      store.interface = {
        state: { ...initialState },
        visibility: { ...visibilityOfControls },
        controlWithUIPanel: { ...controlWithUIPanel },
        hoverDataEvent: null
      }
    },

    setControlVisibility: (store, { name, value }) => {
      store.interface.visibility = {
        ...store.interface.visibility,
        [name]: value
      }
    },

    setControlUIPanel: (store, val) => {
      store.controlUIPanel = val
    }
  },

  actions: {

    toggle: ({ dispatch, getters }, control) => {
      if (getters.interfaceState[control]) dispatch('desactivateControl', control)
      else dispatch('activateControl', control)
    },
    toggleControlWithUIPanel: ({ dispatch, getters }, control) => {
      if (getters.controlWithUIPanel[control]) dispatch('setControlWithUIPanel', { name: control, value: false })
      else dispatch('setControlWithUIPanel', { name: control, value: true })
    },
    activateControl: ({ commit }, control) => commit('setControlStatus', { name: control, value: true }),
    desactivateControl: ({ commit }, control) => commit('setControlStatus', { name: control, value: false }),

    reset: ({ commit }) => commit('resetInterface'),

    showControl: ({ commit }, name) => commit('setControlVisibility', { name, value: true }),
    hideControl: ({ commit }, name) => commit('setControlVisibility', { name, value: false }),

    hoverProject: ({ commit }, hoverDataEvent) => commit('hoverProject', hoverDataEvent),
    unHoverProject: ({ commit, state }, hoverDataEvent) => {
      if (state.interface.hoverDataEvent === hoverDataEvent) commit('unHoverProject')
    },

    setControlWithUIPanel: ({ commit }, { name, value }) => {
      commit('setControlUIPanel', value)
      commit('resetControlPanelStatusExceptBy', name)
      commit('setControlWithUIPanel', { name, value })
    },

    closeControlAndResetUIPanel: ({ commit }) => {
      commit('resetControlPanelStatusExceptBy')
      commit('setControlUIPanel', false)
    }
  },

  getters: {

    interfaceState: state => state.interface.state,

    interfaceVisibility: state => state.interface.visibility,

    getHoverProject: state => state.interface.hoverDataEvent,

    shapesState: state => state.interface.state.shapes,

    controlWithUIPanel: state => state.interface.controlWithUIPanel,

    asuControlUIPanel: state => state.controlUIPanel

  }
}
