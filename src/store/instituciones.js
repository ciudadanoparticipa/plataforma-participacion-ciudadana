import InstitucionesService from '@/services/instituciones'
import { hasValue } from '@/utils/object.js'
import router from '@/router'

export default {
  state: {
    centroMunicipal: {
      allFeatures: [],
      geoJson: {},
      current: null,
      firstTimeId: null
    },
    institucion: {
      allFeatures: [],
      geoJson: {},
      current: null,
      firstTimeId: null
    },
    coberturas: {
      all: {},
      current: null
    }
  },
  mutations: {
    setCentrosMunicipalesAllFeatures: (state, allFeatures) => { state.centroMunicipal = { ...state.centroMunicipal, allFeatures } },
    setCentrosMunicipales: (state, centrosMunicipales) => { state.centroMunicipal = { ...state.centroMunicipal, geoJson: centrosMunicipales } },
    setCentroMunicipal: (state, current) => { state.centroMunicipal = { ...state.centroMunicipal, current } },
    setFirstTimeCentroMunicipalId: (state, firstTimeId) => { state.centroMunicipal = { ...state.centroMunicipal, firstTimeId } },
    setCoberturasDeCentrosMunicipales: (state, { coberturas }) => { state.coberturas = { ...state.coberturas, all: coberturas } },
    setCoberturaDeCentroMunicipal: (state, current) => { state.coberturas = { ...state.coberturas, current } },
    setInstitucionesAllFeatures: (state, allFeatures) => { state.institucion = { ...state.institucion, allFeatures } },
    setInstituciones: (state, instituciones) => { state.institucion = { ...state.institucion, geoJson: instituciones } },
    setInstitucion: (state, current) => { state.institucion = { ...state.institucion, current } },
    setFirstTimeInstitucionId: (state, firstTimeId) => { state.institucion = { ...state.institucion, firstTimeId } }
  },
  actions: {
    setCentrosMunicipales: ({ commit, dispatch }, { centrosMunicipales, shouldFilter = true }) => {
      commit('setCentrosMunicipales', centrosMunicipales)
      if (shouldFilter) dispatch('filterCentrosMunicipales')
    },
    setCentroMunicipal: ({ commit }, centroMunicipal) => commit('setCentroMunicipal', centroMunicipal),
    setCentroMunicipalAndUpdateRoute: ({ dispatch }, { centroMunicipal }) => {
      if (centroMunicipal) router.replace(`/map?ver=refugios&centro_municipal_id=${centroMunicipal.id}`)
      else router.replace(`/map?ver=refugios`)
      dispatch('setCentroMunicipal', centroMunicipal)
    },
    setCentroMunicipalById: ({ commit, dispatch, state }, id) => {
      const centroMunicipal = InstitucionesService.getCentroMunicipalById(state.centroMunicipal.geoJson.features, id)
      if (hasValue(centroMunicipal)) {
        dispatch('setCentroMunicipal', centroMunicipal)
      } else {
        commit('setFirstTimeCentroMunicipalId', parseInt(id))
      }
    },
    setCoberturasDeCentrosMunicipales: ({ commit }, { coberturas, shouldFilter = true }) => commit('setCoberturasDeCentrosMunicipales', { coberturas, shouldFilter }),
    setCoberturaDeCentroMunicipal: ({ commit }, cobertura) => commit('setCoberturaDeCentroMunicipal', cobertura),
    setInstituciones: ({ commit, dispatch }, { instituciones, shouldFilter = true }) => {
      commit('setInstituciones', instituciones)
      if (shouldFilter) dispatch('filterInstituciones')
    },
    setInstitucion: ({ commit }, institucion) => commit('setInstitucion', institucion),
    setInstitucionAndUpdateRoute: ({ dispatch }, { institucion }) => {
      if (institucion) router.replace(`/map?ver=refugios&institucion_id=${institucion.id}`)
      else router.replace(`/map?ver=refugios`)
      dispatch('setInstitucion', institucion)
    },
    setInstitucionById: ({ commit, dispatch, state }, id) => {
      const institucion = InstitucionesService.getInstitucionById(state.institucion.geoJson.features, id)
      if (hasValue(institucion)) {
        dispatch('setInstitucion', institucion)
      } else {
        commit('setFirstTimeInstitucionId', parseInt(id))
      }
    },
    loadCentrosMunicipales: ({ commit, dispatch, state, rootState }) => InstitucionesService.getCentrosMunicipales().then(centrosMunicipales => {
      commit('setCentrosMunicipalesAllFeatures', centrosMunicipales.features)
      if (rootState.controls.interface.state.instituciones) dispatch('setCentrosMunicipales', { centrosMunicipales })
      if (state.centroMunicipal.firstTimeId) {
        const centroMunicipal = InstitucionesService.getCentroMunicipalById(centrosMunicipales.features, state.centroMunicipal.firstTimeId)
        if (hasValue(centroMunicipal)) {
          dispatch('setCentroMunicipal', centroMunicipal)
          commit('setFirstTimeCentroMunicipalId', null)
        }
      }
    }),
    loadCoberturasDeCentrosMunicipales: ({ dispatch }) => InstitucionesService.getCoberturasDeCentrosMunicipales().then(coberturas => dispatch('setCoberturasDeCentrosMunicipales', { coberturas })),
    loadInstituciones: ({ commit, dispatch, state, rootState }) => InstitucionesService.getInstituciones().then(instituciones => {
      commit('setInstitucionesAllFeatures', instituciones.features)
      if (rootState.controls.interface.state.instituciones) dispatch('setInstituciones', { instituciones })
      if (state.institucion.firstTimeId) {
        const institucion = InstitucionesService.getInstitucionById(instituciones.features, state.institucion.firstTimeId)
        if (hasValue(institucion)) {
          dispatch('setInstitucion', institucion)
          commit('setFirstTimeInstitucionId', null)
        }
      }
    }),
    loadAllDataInInstituciones: ({ dispatch }) => {
      dispatch('loadCentrosMunicipales')
      dispatch('loadCoberturasDeCentrosMunicipales')
      dispatch('loadInstituciones')
    },
    activeOrDeactiveInstituciones: ({ dispatch }, controlVal) => {
      if (controlVal) {
        dispatch('loadAllDataInInstituciones')
      }
    },
    filterCentrosMunicipales: ({ commit, state, getters }) => {
      InstitucionesService.filterCentrosMunicipales(state.centroMunicipal.allFeatures, getters.institucionesFilter).then(features => {
        commit('setCentrosMunicipales', {
          ...state.centroMunicipal.geoJson,
          features
        })
      })
    },
    filterInstituciones: ({ commit, state, getters }) => {
      InstitucionesService.filterInstituciones(state.institucion.allFeatures, getters.institucionesFilter).then(features => {
        commit('setInstituciones', {
          ...state.institucion.geoJson,
          features
        })
      })
    }
  },
  getters: {
    centroMunicipal: state => state.centroMunicipal.current,
    centroMunicipalById: state => id => InstitucionesService.getCentroMunicipalById(state.centroMunicipal.allFeatures, id),
    centrosMunicipales: state => state.centroMunicipal.geoJson,
    centrosMunicipalesProperties: state => state.centroMunicipal.geoJson.properties,
    instituciones: state => state.institucion.geoJson,
    institucionesProperties: state => state.institucion,
    institucion: state => state.institucion.current,
    getAllCentrosMunicipales: state => InstitucionesService.getCentrosMunicipales(),
    getInstitucionById: state => id => InstitucionesService.getInstitucionById(state.institucion.allFeatures, id),
    getAllInstituciones: state => InstitucionesService.getAllInstituciones(),
    coberturasCentroMunicipal: state => state.centroMunicipal.current && InstitucionesService.getCoberturaByCentroMunicipal(state.coberturas.all, state.centroMunicipal.current.id)
  }
}
