const initialState = {
  participa: false,
  franja: false,
  viva: false
}

const visibilityOfControls = {
  participa: true,
  franja: true,
  viva: true
}

export default {
  state: {
    interface: {
      state: { ...initialState },
      visibility: { ...visibilityOfControls }
    }
  },
  mutations: {
    toggleInterfaceOption: (store, name) => {
      store.interface.state = {
        ...initialState,
        [name]: !store.interface.state[name]
      }
    },
    resetInterface: (store) => {
      store.interface = {
        state: { ...initialState },
        visibility: { ...visibilityOfControls }
      }
    }
  },
  actions: {
    showPrograms: ({ commit, state }, control) => {
      if (!state.interface.state[name]) {
        commit('toggleInterfaceOption', control)
      }
    },
    resetProgramsInterfaces: ({ commit }) => commit('resetInterface')
  },
  getters: {
    programsState: state => state.interface.state,
    programsVisibility: state => state.interface.visibility
  }
}
