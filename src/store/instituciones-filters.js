import { buildActions, buildMutations } from './common-filters'
import { institucionesFilterObject, INSTITUCIONES_FILTER_MODULE_NAME } from '@/constants'
import { clone } from '@/utils/object'

const moduleName = INSTITUCIONES_FILTER_MODULE_NAME

export default {
  state: {
    filters: {
      instituciones: {
        ...clone(institucionesFilterObject)
      }
    }
  },

  mutations: {
    ...buildMutations(moduleName)
  },

  actions: {
    ...buildActions(moduleName),

    emptyFilterInstitucionesTypes: ({ dispatch }) => dispatch('emptyInstitucionesFilter', { filterName: 'institucionesTypes' }),

    reloadInstitucionesTypes: ({ dispatch }) => dispatch('reloadInstitucionesFilter', { filterName: 'institucionesTypes' }),

    toggleInstitucionesTypes: ({ dispatch }, type) => dispatch('toggleInstitucionesFilter', { institucionesTypes: type })
  },

  getters: {
    institucionesFilter: state => state.filters.instituciones
  }
}
