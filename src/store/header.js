export default {
  state: {
    showHeader: true
  },

  mutations: {
    setShowHeader: (state, showHeader) => { state.showHeader = showHeader }
  },

  actions: {
    setShowHeader: ({ commit }, showHeader) => {
      commit('setShowHeader', showHeader)
    },
    hiddenHeaderInMobileMapView: ({ dispatch, getters }) => {
      if (getters.isMapView || getters.isSolucionDefinitiva || getters.isSolucionACortoPlazo) {
        if (window.innerWidth < 767) {
          dispatch('setShowHeader', false)
        } else {
          dispatch('setShowHeader', true)
        }
      } else {
        dispatch('setShowHeader', true)
      }
    }
  },

  getters: {
    getShowHeader: state => state.showHeader
  }
}
