import RefugiosService from '@/services/refugios'
import { buildActions, buildMutations } from './common-filters'
import { refugioFilterObject, REFUGIO_FILTER_MODULE_NAME } from '@/constants'
import { clone } from '@/utils/object'

const moduleName = REFUGIO_FILTER_MODULE_NAME

export default {
  state: {
    filters: {
      refugios: {
        ...clone(refugioFilterObject),
        search: ''
      }
    }
  },
  mutations: {
    ...buildMutations(moduleName)
  },
  actions: {
    ...buildActions(moduleName),

    emptyFilterRefugiosStatus: ({ dispatch }) => dispatch('emptyRefugiosFilter', { filterName: 'refugiosStatus' }),
    emptyFilterRefugiosZonas: ({ dispatch }) => dispatch('emptyRefugiosFilter', { filterName: 'zonas' }),
    emptyFilterRefugiosComposicionFamiliar: ({ dispatch }) => dispatch('emptyRefugiosFilter', { filterName: 'composicionFamiliar' }),
    emptyFilterRefugiosBarrios: ({ dispatch }) => dispatch('emptyRefugiosFilter', { filterName: 'barrios' }),

    reloadRefugiosStatus: ({ dispatch }) => dispatch('reloadRefugiosFilter', { filterName: 'refugiosStatus' }),
    reloadRefugiosZonas: ({ dispatch }) => dispatch('reloadRefugiosFilter', { filterName: 'zonas' }),
    reloadRefugiosComposicionFamiliar: ({ dispatch }) => dispatch('reloadRefugiosFilter', { filterName: 'composicionFamiliar' }),
    reloadRefugiosBarrios: ({ dispatch }) => dispatch('reloadRefugiosFilter', { filterName: 'barrios' }),

    toggleRefugiosStatus: ({ dispatch }, refugiosStatus) => dispatch('toggleRefugiosFilter', { refugiosStatus }),
    toggleRefugiosZonas: ({ dispatch }, zonas) => dispatch('toggleRefugiosFilter', { zonas }),
    toggleRefugiosComposicionFamiliar: ({ dispatch }, composicionFamiliar) => dispatch('toggleRefugiosFilter', { composicionFamiliar }),
    toggleRefugiosBarrios: ({ dispatch }, barrios) => dispatch('toggleRefugiosFilter', { barrios }),

    searchRefugiosFilter: ({ dispatch }, search) => dispatch('setRefugiosFilter', { search })
  },
  getters: {
    refugiosSearched: (state, getters) =>
      RefugiosService.searchRefugios(getters.refugiosCensados, getters.refugioSearchText),
    refugiosFilter: state => state.filters.refugios,
    refugioSearchText: state => state.filters.refugios.search
  }
}
