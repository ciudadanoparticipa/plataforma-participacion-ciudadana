import { projectFilterObject, refugioFilterObject, institucionesFilterObject } from '@/constants'
import { capitalize } from '@/utils/string-utils'

const data = {
  ...projectFilterObject,
  ...refugioFilterObject,
  ...institucionesFilterObject
}

const getDispatchName = moduleName => `set${capitalize(moduleName)}Filter`

export const buildMutations = moduleName => ({

  [getDispatchName(moduleName)]: (state, { filterName, filterValue }) => {
    state.filters = {
      ...state.filters,
      [filterName]: filterValue
    }
  }

})

export const buildActions = moduleName => ({

  [getDispatchName(moduleName)]: ({ commit, state }, params) => {
    Object.keys(params).forEach(key => {
      const value = params[key]
      commit(getDispatchName(moduleName), {
        filterName: moduleName,
        filterValue: {
          ...state.filters[moduleName],
          [key]: value
        }
      })
    })
  },

  [`toggle${capitalize(moduleName)}Filter`]: ({ dispatch, state }, params) => {
    Object.keys(params).forEach(key => {
      let filterList
      const array = state.filters[moduleName][key]
      if (array.indexOf(params[key]) > -1) {
        filterList = array.filter(s => s !== params[key])
      } else {
        filterList = array.concat([params[key]])
      }
      dispatch(getDispatchName(moduleName), { [key]: filterList })
    })
  },

  [`reload${capitalize(moduleName)}Filter`]: ({ dispatch, state }, { filterName, fieldName }) => {
    if (state.filters[moduleName][filterName].length === data[filterName].length) return
    dispatch(getDispatchName(moduleName), { [filterName]: fieldName ? data[filterName].map(t => t[fieldName]) : data[filterName] })
  },

  [`empty${capitalize(moduleName)}Filter`]: ({ dispatch, state }, { filterName }) => {
    if (state.filters[moduleName][filterName].length === 0) return
    dispatch(getDispatchName(moduleName), { [filterName]: [] })
  }

})
